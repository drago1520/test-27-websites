#!/bin/bash

# Loop from 1 to 27
for i in {1..27}
do
   # Assuming you want to name your branches 'site1', 'site2', ..., 'site27'
   branch_name="site$i"

   # Checkout a new branch based on main
   git checkout -b $branch_name main

   # Optionally, push the new branch to remote. Remove these lines if you don't want to push immediately.
   git push -u origin $branch_name

   # Switch back to the main branch before starting the next iteration
   git checkout main
done

echo "All 27 branches have been created and pushed!"
