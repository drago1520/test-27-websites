#!/bin/bash

# Array of your 27 website branches
BRANCHES=(
"site1"
"site2"
"site3"
"site4"
"site5"
"site6"
"site7"
"site8"
"site9"
"site10"
"site11"
"site12"
"site13"
"site14"
"site15"
"site16"
"site17"
"site18"
"site19"
"site20"
"site21"
"site22"
"site23"
"site24"
"site25"
"site26"
"site27"
)

# Fetch the latest changes from the origin
git fetch origin

# Loop through each branch and merge changes from the main branch
for branch in "${BRANCHES[@]}"; do
  echo "Updating $branch from main..."
  
  # Check out the branch
  git checkout $branch

  # Pull changes from the main branch
  # Use --no-edit to automatically accept the default commit message for the merge
  git pull --no-edit origin main

  # Handle any conflicts or issues that might arise here
  
  # Optionally, push the updated branch back to the remote repository
  # Remove this line if you don't want to push immediately
  # git push origin $branch
done

# Switch back to the main branch
git checkout main

git push --all

echo "All branches are updated from main!"
