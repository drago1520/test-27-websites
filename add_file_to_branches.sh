#!/bin/bash

# Array of your 27 website branches
BRANCHES=(
  "site1"
  "site2"
  "site3"
  "site4"
  "site5"
  "site6"
  "site7"
  "site8"
  "site9"
  "site10"
  "site11"
  "site12"
  "site13"
  "site14"
  "site15"
  "site16"
  "site17"
  "site18"
  "site19"
  "site20"
  "site21"
  "site22"
  "site23"
  "site24"
  "site25"
  "site26"
  "site27"
)

# Loop through each branch and add a file
for branch in "${BRANCHES[@]}"; do
  echo "Adding a file to $branch..."

  # Check out the branch
  git checkout $branch

  # Create a new file
  # Here, I'm just creating a text file. Adjust as needed for your file type and content.
  echo "This is some content for the branch $branch" > "file_for_$branch.txt"

  # Add the new file to the staging area
  git add "file_for_$branch.txt"

  # Commit the new file
  git commit -m "Added a custom file for $branch"

  # Optionally, push the commit to the remote repository
  # Remove this line if you don't want to push immediately
  git push origin $branch

  # Remove the file after committing to avoid it being present in other branches
  rm "file_for_$branch.txt"

done

# Switch back to the main branch
git checkout main

# Clean up any remaining untracked files (just in case)
git clean -f

echo "All branches have been updated with a new file!"
